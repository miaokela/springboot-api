package com.quickstart;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;

import com.quickstart.entity.User;
import com.quickstart.service.IUserService;

@SpringBootTest(classes = Application.class)
public class QuickStartTest {

    private static final Logger logger = LoggerFactory.getLogger(QuickStartTest.class);

    @Autowired
    private IUserService userService;

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    @Test
    public void testInsertService() {
        User user = new User();
        user.setName("quickstart");
        user.setAge(30);
        user.setEmail("2222@qq.com");
        userService.save(user);
    }

    @Test
    public void testSelectService() {
        List<User> list = userService.list();

        list.forEach(item -> {
            logger.info("=========" + item.getName());
        });
    }

    @Test
    public void testInsertRedis() {
        redisTemplate.opsForValue().set("name", "miaokela");
    }
}
