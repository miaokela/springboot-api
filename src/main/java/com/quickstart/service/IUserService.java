package com.quickstart.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.quickstart.entity.User;

public interface IUserService extends IService<User> {

    Page<User> selectUserPage(int page, int size);

}
