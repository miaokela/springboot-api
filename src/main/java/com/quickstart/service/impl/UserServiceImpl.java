package com.quickstart.service.impl;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.quickstart.entity.User;
import com.quickstart.mapper.UserMapper;
import com.quickstart.service.IUserService;

@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {
    public Page<User> selectUserPage(int page, int pageSize) {
        Page<User> userPage = new Page<>(page, pageSize);
        return this.page(userPage);
    }
}
