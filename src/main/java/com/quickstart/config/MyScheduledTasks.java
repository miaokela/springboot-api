package com.quickstart.config;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class MyScheduledTasks {

    @Scheduled(fixedRate = 5000)
    public void resportCurrentTime() {
        System.out.println("当前时间: " + System.currentTimeMillis());
    }

    @Scheduled(cron = "0 0 1 * * ?")
    public void executeDaily() {
        System.out.println("执行日常任务");
    }
}
