package com.quickstart.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.quickstart.pojo.LoginRequest;
import com.quickstart.utils.ApiResponse;
import com.quickstart.utils.JwtUtil;

import io.swagger.v3.oas.annotations.parameters.RequestBody;

@RestController
public class AuthController {
    // 注入AuthenticationManager
    // 先实现Authentication接口
    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtUtil jwtUtil;

    private static final Logger logger = LoggerFactory.getLogger(AuthController.class);

    @PostMapping("/login")
    public ApiResponse<String> login(@RequestBody LoginRequest loginRequest) {
        try {
            // 用户认证
            // Spring Security的AuthenticationManager会通过AuthenticationProvider
            //（通常是DaoAuthenticationProvider）间接调用UserDetailsService来加载用户信息
            Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            loginRequest.getUsername(),
                            loginRequest.getPassword()));
            // 建立应用上下文
            SecurityContextHolder.getContext().setAuthentication(authentication);

            // 获取用户信息
            UserDetails userDetails = (UserDetails) authentication.getPrincipal();
            System.out.println(userDetails);
            
            // 颁布JWT
            String token = jwtUtil.generateToken(userDetails);
            System.out.println("-----");
            logger.info("用户" + userDetails.getUsername() + "登录成功");

            return new ApiResponse<>(HttpStatus.OK.value(), "操作成功", token);
        } catch (Exception e) {
            return new ApiResponse<>(HttpStatus.UNAUTHORIZED.value(), "用户名或者密码错误", null);
        }
    }

}
