# 使用官方Java运行时环境作为基础镜像
FROM openjdk:8-jdk-alpine

# 指定维护者信息
LABEL maintainer="yourname@example.com"

# 添加一个卷，指向外部的/tmp，因为Spring Boot使用的内嵌Tomcat容器默认使用/tmp作为工作目录
VOLUME /tmp

# 将jar文件添加到容器中
ADD target/myproject-1.0-SNAPSHOT.jar myproject-1.0-SNAPSHOT.jar

# 声明运行时容器提供服务端口
EXPOSE 8080

# 配置容器启动后执行的命令
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/myproject-1.0-SNAPSHOT.jar"]
